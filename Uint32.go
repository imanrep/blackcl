package blackcl

/*
#cgo CFLAGS: -I CL
#cgo !darwin LDFLAGS: -lOpenCL
#cgo darwin LDFLAGS: -framework OpenCL

#define CL_TARGET_OPENCL_VERSION 300

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
*/
import "C"
import (
	"errors"
	"unsafe"
)

//Uint32 is a memory buffer on the device that holds []uint32
type Uint32 struct {
	buf *buffer
}

//Length the length of the uint32
func (v *Uint32) Length() int {
	return v.buf.size / 4
}

//Release releases the buffer on the device
func (v *Uint32) Release() error {
	return v.buf.Release()
}

//NewVector allocates new vector buffer with specified length
func (d *Device) NewUint32(length int) (*Uint32, error) {
	size := length * 4
	buf, err := newBuffer(d, size)
	if err != nil {
		return nil, err
	}
	return &Uint32{buf: &buffer{memobj: buf, device: d, size: size}}, nil
}

//Copy copies the uint32 data from host data to device buffer
//it's a non-blocking call, channel will return an error or nil if the data transfer is complete
func (v *Uint32) Copy(data []uint32) <-chan error {
	if v.Length() != len(data) {
		ch := make(chan error, 1)
		ch <- errors.New("vector length not equal to data length")
		return ch
	}
	return v.buf.copy(len(data)*4, unsafe.Pointer(&data[0]))
}

//Data gets uint32 data from device, it's a blocking call
func (v *Uint32) Data() ([]uint32, error) {
	data := make([]uint32, v.buf.size/4)
	err := toErr(C.clEnqueueReadBuffer(
		v.buf.device.queue,
		v.buf.memobj,
		C.CL_TRUE,
		0,
		C.size_t(v.buf.size),
		unsafe.Pointer(&data[0]),
		0,
		nil,
		nil,
	))
	if err != nil {
		return nil, err
	}
	return data, nil
}

//Map applies an map kernel on all elements of the vector
func (v *Uint32) Map(k *Kernel) <-chan error {
	return k.Global(v.Length()).Local(1).Run(v)
}
